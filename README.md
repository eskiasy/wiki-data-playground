# Wiki Data Playground

A simple script to generate the `n`th most frequently used word in any country's Wikipedia page.

## How to Use
- Install required modules found in the `requirements.txt` file.
- Edit the line `for n in range(1, 3):` as per your needs to generate a list of the top `n` used terms in all country's Wikipedia pages.
  For eg. `for n in range(1, 3):` will generate the 1st and 2nd most frequently used terms.
          `for n in range(7, 10):` will generate the 7st, 8th and 9th most frequently used terms and so on.
- Just run it as you would run a normal python script from your terminal
    `python3 Wiki_play.py` or `./Wiki_play.py`
- It will generate HTML files with the file name being `n`.
- Open the HTML file in your web browser and `hover` over a country to view the `n`th term displayed next to the country's name.

## Note
- The first few top terms might not be as useful as they mostly consist of words like 'retrived', 'archived' or the ountry's name itself.
  Hence you can start somewhere from 6th or 7th for a more useful result set.
- The process might be lengthy as it will iterate through all countries.
  To only generate for a specific country you can
    - print out countries list after `countries = world["name"].tolist()`
    - Remove the For loop (`for i, country in enumerate(countries):`) and the print statement after it and specify the country of your choice as for eg. `country = "Ethiopia"`

## Enjoy!
