#!/usr/bin/python3
import requests
from bs4 import BeautifulSoup
import nltk
import geopandas as gpd
import folium

# A one time download of NLTK libraries [Comment out the next two lines after a first run]
nltk.download("punkt")
nltk.download("stopwords")

from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords


# Step 1: Get a list of all countries in the world
world = gpd.read_file(gpd.datasets.get_path("naturalearth_lowres"))
countries = world["name"].tolist()

# Step 2: Create an empty dictionary to store the most frequent word for each country
most_common_words = {}

# Step 3: Loop through each country and scrape the Wikipedia page to find the most frequent word
# Change the range to get the nth most common word
for n in range(1, 3):
    print("Top {}th Term".format(str(n)))
    for i, country in enumerate(countries):
        print(colored("Tokenizing {}/{} -- {} ... ".format(i,
                                                           len(countries), country), "green"))
        url = f"https://en.wikipedia.org/wiki/{country}"
        try:
            response = requests.get(url)
            soup = BeautifulSoup(response.text, "html.parser")
            text = soup.get_text()
        except:
            continue

        # Tokenize words
        words = word_tokenize(text)
        words = [word.lower() for word in words if word.isalpha()]
        words = [
            word for word in words if word not in stopwords.words("english")]
        word_freq = nltk.FreqDist(words)

        # check if there are at least n distinct words
        if len(word_freq) >= n:
            nth_most_common_word = word_freq.most_common(n)[n - 1][0]

        # nth most common word
        most_common_words[country] = nth_most_common_word

    # Step 4: Create a map and add a tooltip for each country showing the most frequent word
    m = folium.Map(location=[0, 0], zoom_start=2)
    for index, row in world.iterrows():
        country_name = row["name"]
        if country_name in most_common_words:
            most_common_word = most_common_words[country_name]
            tooltip_text = f"{country_name}: {most_common_word}"
            tooltip = folium.Tooltip(tooltip_text)
            folium.GeoJson(
                data=row["geometry"],
                tooltip=tooltip,
                style_function=lambda x: {
                    "fillColor": "lightgray",
                    "color": "black",
                    "weight": 1
                }
            ).add_to(m)

    # Step 5: Save the map as HTML in the current directory

    html_filename = "{}-map.html".format(n)
    m.save(html_filename)
